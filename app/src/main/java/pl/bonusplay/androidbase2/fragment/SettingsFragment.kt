package pl.bonusplay.androidbase2.fragment

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import pl.bonusplay.androidbase2.R

class SettingsFragment : PreferenceFragmentCompat()
{
	override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?)
	{
		setPreferencesFromResource(R.xml.settings, rootKey)
	}
}