package pl.bonusplay.androidbase2.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import pl.bonusplay.androidbase2.R
import pl.bonusplay.androidbase2.fragment.HomeFragment
import pl.bonusplay.androidbase2.fragment.SettingsFragment

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener
{
	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		setSupportActionBar(toolbar)

		val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
		drawer_layout.addDrawerListener(toggle)
		toggle.syncState()

		nav_view.setNavigationItemSelectedListener(this)

		setFragment(HomeFragment())
	}

	override fun onBackPressed()
	{
		if (drawer_layout.isDrawerOpen(GravityCompat.START))
			drawer_layout.closeDrawer(GravityCompat.START)
		else
			super.onBackPressed()
	}

	override fun onNavigationItemSelected(item: MenuItem): Boolean
	{
		val fragment = when (item.itemId)
		{
			R.id.nav_home -> HomeFragment()
			R.id.nav_settings -> SettingsFragment()
			else -> throw RuntimeException("Unknown menu item")
		}

		drawer_layout.closeDrawer(GravityCompat.START)
		setFragment(fragment)
		return true
	}

	private fun setFragment(fragment: Fragment)
	{
		supportFragmentManager
				.beginTransaction()
				.replace(R.id.fragment_container, fragment)
				.commit()
	}
}