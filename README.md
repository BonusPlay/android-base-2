# Androd-Base 2
Base Project structure for Android. Based around 1 activity with navigation drawer and changeable content via fragments.
Feel free to use.

## Features
- [x] Kotlin
- [x] AndroidX
- [x] 1 activity app (fragments)
- [ ] Tests

## FAQ
**Q: Why do you say** ***"I reserve the right to revoke this use of this license at any time, for any reason, and at the sole discretion of mine."*** **in the license?**

*Just as a backup to to punish anyone that breaks the license by being a dick.*